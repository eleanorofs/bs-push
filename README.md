# bs-push

This package closely wraps the
[Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API). 
It does not include the Push API Service Worker Additions. Instead,
it is a dependency of the `bs-service-worker` package. It has not been
exhaustively tested. 

## Installation
`npm i bs-push`

## Implemented
- [X] [EventInitDict](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [EventType](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [PushEvent](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [ ] [PushManager](https://developer.mozilla.org/en-US/docs/Web/API/PushManager)
- [X] [PushManagerPermissionState](https://developer.mozilla.org/en-US/docs/Web/API/PushManager/permissionState)
- [X] [PushMessageData](https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData)
- [X] [PushSubscription](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription)
- [X] [SubscriptionOptions](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription/options)

## Notes
### 'data
`'data` is a frequent type parameter. It represents the untyped data object
passed into the eventInitDict of the PushEvent constructor. 

### PushMessageData
There's currently no BuckleScript `Blob` type binding that I'm aware of, so
rather than find a workaround, I'm just leaving that one as an exercise for 
the reader. 

## License

This software is available under two licenses. 

* an adaptation of the Do No Harm license which I've called the No Violence
license. 
* the MIT license.

Both are available in this directory. 


## For further reading
I *strongly* recommend you check out my 
[catch-all documentation](https://webbureaucrat.gitlab.io/posts/issues-and-contribution/)
on my 
projects. It describes how to get in touch with me if you have any questions, 
how to contribute code, coordinated disclosure of security vulnerabilities, 
and more. It will be regularly updated with any information I deem relevant
to my side projects. 

