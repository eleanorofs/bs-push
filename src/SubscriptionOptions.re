type t =
  { userVisibleOnly: bool
  , applicationServerKey: string
  };