/* Note: I have no plans to implement deprecated methods and properties, but
 * if you need them, by all means open an issue and submit a pull request. I'm
 * open to adding them if there's a need. */

type t;

/* properties */
[@bs.get] external supportedContentEncodings: t => list(string)
  = "supportedContentEncodings";

/* methods */
[@bs.send] external getSubscription: t => Js.Promise.t(PushSubscription.t)
  = "getSubscription";

[@bs.send]
external permissionState: t => Js.Promise.t(string)
  = "permissionState";

[@bs.send]
external subscribe: t => Js.Promise.t(PushSubscription.t) = "subscribe";
