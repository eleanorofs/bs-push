type t('data);

[@bs.send] external arrayBuffer: t('data) => Js_typed_array.array_buffer
  = "arrayBuffer";

// TODO: What do I do with `blob`?

[@bs.send] external json: t('data) => 'data = "json"; 

[@bs.send] external text: t('data) => string = "text";
