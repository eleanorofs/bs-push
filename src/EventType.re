[@bs.deriving jsConverter]
type t  = [
  | `push
  | `pushsubscriptionchange
];
